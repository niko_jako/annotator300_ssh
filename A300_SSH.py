## Begin ControlScript Import --------------------------------------------------
from extronlib import event, Version
from extronlib.device import eBUSDevice, ProcessorDevice, UIDevice
from extronlib.interface import (AnnotatorInterface,CircuitBreakerInterface, ContactInterface,
    DigitalInputInterface, DigitalIOInterface, EthernetClientInterface,
    EthernetServerInterfaceEx, FlexIOInterface, IRInterface, PoEInterface,
    RelayInterface, SerialInterface, SWACReceptacleInterface, SWPowerInterface,
    VolumeInterface)
from extronlib.ui import Button, Knob, Label, Level, Slider
from extronlib.system import Clock, MESet, Timer, Wait

print(Version())
tli = UIDevice('PanelAlias')

btn111 = Button(tli, 111)
@event(btn111, 'Pressed')
def btnpress111(btn, state):
    print(btn.ID, state)
    drawpoint(tcp, tcpalias)
    
btn111 = Button(tli, 4)
@event(btn111, 'Pressed')
def btnpress111(btn, state):
    print(btn.ID, state)
    drawpointssh(ssh, tcpalias)
    
    
btn2 = Button(tli, 2)
@event(btn2, 'Pressed')
def btnpress2(btn, state):
    print(btn.ID, state)
    
    
btn3 = Button(tli, 3)
@event(btn3, 'Pressed')
def btnpress3(btn, state):
    print(btn.ID, state)
    clearpoint(tcp)
    

ip = '10.113.139.43'
username = 'admin'
password = 'extron'
tcpalias = 'tcpalias'
sshalias = 'sshalias'
start = 130
width = 100
height =  100
step = 5

start2 = 230


tcp = EthernetClientInterface(ip, 23)
tcp.Connect()
ssh = EthernetClientInterface(ip, 22023,'SSH',('admin', 'extron'))
#ssh.Connect()
#ssh = AnnotatorInterface('10.113.139.43', 22023, ('admin', 'extron'))
ssh.Connect()
ssh.Send('\x1b0echo\r')

@event(tcp, 'ReceiveData')
def tcpReceiveData(interface, rcvstring):
    print('tcp', rcvstring)
    
    if b'Password' in rcvstring:
        interface.Send(password+'\r')
    elif b'Login Administrator' in rcvstring:
        interface.Send('\x1b0echo\r')
        
        
def drawpoint(interface, alias):
    interface.Send('w{}APID\r'.format(alias))
    for w in range(start, start+width, step):
        for h in range(start, start+height, step):
            interface.Send('w{0:0>4}{1:0>4}APNT\r'.format(w, h))
            
    interface.Send('wASTP\r')
    
    
def drawpointssh(interface, alias):
    interface.Send('w{}APID\r'.format(alias))
    for w in range(start2, start2+width, step):
        for h in range(start2, start2+height, step):
            interface.Send('w{0:0>4}{1:0>4}APNT\r'.format(w, h))
            
    interface.Send('wASTP\r')    
    
def clearpoint(interface):
    interface.Send('\x1b0EDIT\r')       


def Initialize():
    pass

## Event Definitions -----------------------------------------------------------

## End Events Definitions-------------------------------------------------------

Initialize()
